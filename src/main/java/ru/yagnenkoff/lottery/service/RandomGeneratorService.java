package ru.yagnenkoff.lottery.service;

public interface RandomGeneratorService {
    long generateWinner(long numberOfParticipants);
    long generateWinningAmount();
}
