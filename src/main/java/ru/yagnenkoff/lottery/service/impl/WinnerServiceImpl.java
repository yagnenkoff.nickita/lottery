package ru.yagnenkoff.lottery.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.yagnenkoff.lottery.domain.WinnerResponse;
import ru.yagnenkoff.lottery.entity.WinnerEntity;
import ru.yagnenkoff.lottery.repository.WinnerRepository;
import ru.yagnenkoff.lottery.service.WinnerService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WinnerServiceImpl implements WinnerService {

    private final WinnerRepository winnerRepository;

    @Override
    public List<WinnerResponse> getWinners() {
        return winnerRepository.findAll().stream()
                .map(WinnerEntity::asWinnerResponse)
                .collect(Collectors.toList());
    }
}
