package ru.yagnenkoff.lottery.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yagnenkoff.lottery.domain.WinnerResponse;
import ru.yagnenkoff.lottery.entity.WinnerEntity;
import ru.yagnenkoff.lottery.exception.GameException;
import ru.yagnenkoff.lottery.repository.ParticipantRepository;
import ru.yagnenkoff.lottery.repository.WinnerRepository;
import ru.yagnenkoff.lottery.service.GameService;
import ru.yagnenkoff.lottery.service.RandomGeneratorService;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class GameServiceImpl implements GameService {

    private final ParticipantRepository participantRepository;
    private final WinnerRepository winnerRepository;
    private final RandomGeneratorService generatorService;

    private final static long MIN_PARTICIPANTS = 2;

    @Override
    @Transactional
    public WinnerResponse startGame() {
        log.info("Game started");
        final var winningAmount = generatorService.generateWinningAmount();
        final var winner = Optional.of(participantRepository.count())
                .filter(count -> count >= MIN_PARTICIPANTS)
                .map(generatorService::generateWinner)
                .map(participantRepository::getParticipantEntityByRowNumber)
                .orElseThrow(GameException::new);

        final var winnerEntity = new WinnerEntity();
        winnerEntity.setName(winner.getName());
        winnerEntity.setAge(winner.getAge());
        winnerEntity.setCity(winner.getCity());
        winnerEntity.setWinningAmount(winningAmount);

        final var winnerResultEntity = winnerRepository.save(winnerEntity);
        final var winnerResponse = winnerResultEntity.asWinnerResponse();
        log.info("Winner card: {}", winnerResponse);
        participantRepository.deleteAll();
        return winnerResponse;
    }
}
