package ru.yagnenkoff.lottery.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.yagnenkoff.lottery.exception.RandomRequestException;
import ru.yagnenkoff.lottery.service.RandomGeneratorService;

import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RandomGeneratorServiceImpl implements RandomGeneratorService {

    private static final String RANDOM_GENERATOR_URL = "/integers/?num=1&min=1&max={max}&col=1&base=10&format=plain&rnd=new";
    private static final int MAX_WIN = 1000;
    private static final String MAX = "max";

    private final RestTemplate restTemplate;

    @Override
    public long generateWinner(long participantsCount) {
        return exchangeRequest(participantsCount);
    }

    @Override
    public long generateWinningAmount() {
        return exchangeRequest(MAX_WIN);
    }

    private long exchangeRequest(long max) {
        final var response = restTemplate.getForObject(RANDOM_GENERATOR_URL, String.class, generateParams(max));
        final var result = Optional.ofNullable(response)
                .orElseThrow(RandomRequestException::new);
        return Long.parseLong(result.trim());
    }

    private Map<String, String> generateParams(long max) {
        return Map.of(MAX, String.valueOf(max));
    }
}
