package ru.yagnenkoff.lottery.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.yagnenkoff.lottery.domain.Participant;
import ru.yagnenkoff.lottery.entity.ParticipantEntity;
import ru.yagnenkoff.lottery.repository.ParticipantRepository;
import ru.yagnenkoff.lottery.service.ParticipantService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ParticipantServiceImpl implements ParticipantService {

    private final ParticipantRepository participantRepository;

    @Override
    public void register(Participant participant) {
        final var participantEntity = new ParticipantEntity();
        participantEntity.setName(participant.name());
        participantEntity.setAge(participant.age());
        participantEntity.setCity(participant.city());
        participantRepository.save(participantEntity);
        log.info("Participant registered");
    }

    @Override
    public List<Participant> getParticipants() {
        return participantRepository.findAll().stream()
                .map(ParticipantEntity::asParticipant)
                .collect(Collectors.toList());
    }
}
