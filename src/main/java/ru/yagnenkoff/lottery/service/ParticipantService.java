package ru.yagnenkoff.lottery.service;

import ru.yagnenkoff.lottery.domain.Participant;

import java.util.List;

public interface ParticipantService {
    void register(Participant participant);
    List<Participant> getParticipants();
}
