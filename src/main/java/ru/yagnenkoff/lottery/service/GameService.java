package ru.yagnenkoff.lottery.service;

import ru.yagnenkoff.lottery.domain.WinnerResponse;

public interface GameService {
    WinnerResponse startGame();
}
