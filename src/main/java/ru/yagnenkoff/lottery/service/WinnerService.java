package ru.yagnenkoff.lottery.service;

import ru.yagnenkoff.lottery.domain.WinnerResponse;

import java.util.List;

public interface WinnerService {
    List<WinnerResponse> getWinners();
}
