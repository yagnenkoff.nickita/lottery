package ru.yagnenkoff.lottery.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;


@Configuration
public class RestTemplateConfiguration {

    private final String rootUri;
    private final Duration readTimeout;
    private final Duration connectTimeout;

    @Autowired
    public RestTemplateConfiguration(@Value("${lottery.baseUrl}") String rootUri,
                                     @Value("${lottery.readTimeout:1S}") Duration readTimeout,
                                     @Value("${lottery.connectTimeout:1S}") Duration connectTimeout) {
        this.rootUri = rootUri;
        this.readTimeout = readTimeout;
        this.connectTimeout = connectTimeout;
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.rootUri(rootUri)
                .setConnectTimeout(connectTimeout)
                .setReadTimeout(readTimeout)
                .build();
    }
}
