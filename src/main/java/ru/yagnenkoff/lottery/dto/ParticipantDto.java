package ru.yagnenkoff.lottery.dto;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ru.yagnenkoff.lottery.domain.Participant;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class ParticipantDto {
    @NotBlank
    @Size(max = 64)
    private final String name;
    @NotNull
    @Min(18)
    private final Integer age;
    @NotBlank
    @Size(max = 100)
    private final String city;

    public ParticipantDto(Participant participant) {
        this(participant.name(), participant.age(), participant.city());
    }

    public Participant toDomain() {
        return new Participant(name, age, city);
    }
}


