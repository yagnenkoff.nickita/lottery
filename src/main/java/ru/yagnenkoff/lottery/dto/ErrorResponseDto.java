package ru.yagnenkoff.lottery.dto;

import lombok.*;

import java.time.Instant;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class ErrorResponseDto {
    private final Integer statusCode;
    private final String message;
    private final Instant timestamp;
}
