package ru.yagnenkoff.lottery.dto;

import lombok.*;
import ru.yagnenkoff.lottery.domain.WinnerResponse;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class WinnerResponseDto {
    private final String name;
    private final Integer age;
    private final String city;
    private final Long winningAmount;

    public WinnerResponseDto(WinnerResponse winnerResponse) {
        this(winnerResponse.name(), winnerResponse.age(), winnerResponse.city(), winnerResponse.winningAmount());
    }
}
