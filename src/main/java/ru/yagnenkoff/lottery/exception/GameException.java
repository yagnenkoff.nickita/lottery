package ru.yagnenkoff.lottery.exception;

public class GameException extends RuntimeException {
    public GameException() {
        super("Draw error");
    }
}
