package ru.yagnenkoff.lottery.exception;

public class RandomRequestException extends RuntimeException {
    public RandomRequestException() {
        super("Random request execution error");
    }
}
