package ru.yagnenkoff.lottery.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.yagnenkoff.lottery.domain.WinnerResponse;

import javax.persistence.*;

/**
 * Entity to store winners.
 */
@Entity
@Table(name = "LOTTERY_WINNERS")
@Getter
@Setter
@NoArgsConstructor
public class WinnerEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "CITY")
    private String city;
    @Column(name = "WINNING_AMOUNT")
    private Long winningAmount;

    public WinnerResponse asWinnerResponse() {
        return new WinnerResponse(name, age, city, winningAmount);
    }
}
