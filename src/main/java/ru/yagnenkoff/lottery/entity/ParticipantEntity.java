package ru.yagnenkoff.lottery.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.yagnenkoff.lottery.domain.Participant;

import javax.persistence.*;

/**
 * Entity to store participants in the draw.
 */
@Entity
@Table(name = "LOTTERY_PARTICIPANTS")
@Getter
@Setter
@NoArgsConstructor
public class ParticipantEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "CITY")
    private String city;

    public Participant asParticipant() {
        return new Participant(name, age, city);
    }
}
