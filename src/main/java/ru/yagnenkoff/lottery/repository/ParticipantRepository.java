package ru.yagnenkoff.lottery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.yagnenkoff.lottery.entity.ParticipantEntity;

public interface ParticipantRepository extends JpaRepository<ParticipantEntity, Long> {
    @Query(value = "SELECT id, name, age, city FROM " +
            "(SELECT *, row_number() over () AS row_number " +
            "FROM lottery_participants) as sq " +
            "WHERE row_number = :rowNumber",
            nativeQuery = true)
    ParticipantEntity getParticipantEntityByRowNumber(long rowNumber);
}
