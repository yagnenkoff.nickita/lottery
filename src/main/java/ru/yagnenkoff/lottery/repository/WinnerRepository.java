package ru.yagnenkoff.lottery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yagnenkoff.lottery.entity.WinnerEntity;

public interface WinnerRepository extends JpaRepository<WinnerEntity, Long> {
}
