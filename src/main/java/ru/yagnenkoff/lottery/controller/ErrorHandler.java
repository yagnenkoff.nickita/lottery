package ru.yagnenkoff.lottery.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import ru.yagnenkoff.lottery.dto.ErrorResponseDto;
import ru.yagnenkoff.lottery.exception.GameException;
import ru.yagnenkoff.lottery.exception.RandomRequestException;

import java.time.Instant;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponseDto handleGameException(GameException ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDto(INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Instant.now());
    }

    @ExceptionHandler
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponseDto handleValidationException(RandomRequestException ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDto(BAD_REQUEST.value(), ex.getMessage(), Instant.now());
    }

    @ExceptionHandler
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponseDto handleInternalException(Exception ex, WebRequest request) {
        logException(request, ex);
        return new ErrorResponseDto(INTERNAL_SERVER_ERROR.value(), ex.getMessage(), Instant.now());
    }

    private void logException(WebRequest request, Exception ex) {
        log.error(request.getDescription(false), ex);
    }
}

