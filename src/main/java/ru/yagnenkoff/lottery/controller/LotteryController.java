package ru.yagnenkoff.lottery.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.yagnenkoff.lottery.annotation.LotteryApi;
import ru.yagnenkoff.lottery.dto.ParticipantDto;
import ru.yagnenkoff.lottery.dto.WinnerResponseDto;
import ru.yagnenkoff.lottery.service.GameService;
import ru.yagnenkoff.lottery.service.ParticipantService;
import ru.yagnenkoff.lottery.service.WinnerService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@LotteryApi
@RestController
@RequiredArgsConstructor
public class LotteryController {

    private final ParticipantService participantService;
    private final WinnerService winnerService;
    private final GameService gameService;

    @PostMapping(RequestMap.REGISTER_REQUEST_URL)
    public void register(@RequestBody @Valid ParticipantDto participant) {
        participantService.register(participant.toDomain());
    }

    @GetMapping(RequestMap.GET_PARTICIPANTS_REQUEST_URL)
    public List<ParticipantDto> getParticipants() {
        return participantService.getParticipants().stream()
                .map(ParticipantDto::new)
                .collect(Collectors.toList());
    }

    @GetMapping(RequestMap.START_GAME_REQUEST_URL)
    public WinnerResponseDto startGame() {
        return new WinnerResponseDto(gameService.startGame());
    }

    @GetMapping(RequestMap.GET_WINNERS_REQUEST_URL)
    public List<WinnerResponseDto> getWinners() {
        return winnerService.getWinners().stream()
                .map(WinnerResponseDto::new)
                .collect(Collectors.toList());
    }
}
