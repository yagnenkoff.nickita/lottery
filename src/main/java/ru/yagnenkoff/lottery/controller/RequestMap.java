package ru.yagnenkoff.lottery.controller;

public interface RequestMap {
    String REGISTER_REQUEST_URL = "/participant";
    String GET_PARTICIPANTS_REQUEST_URL = "/participant";
    String START_GAME_REQUEST_URL = "/start";
    String GET_WINNERS_REQUEST_URL = "/winners";
}
