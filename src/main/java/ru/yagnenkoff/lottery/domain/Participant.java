package ru.yagnenkoff.lottery.domain;

public record Participant(String name, Integer age, String city) {
}
