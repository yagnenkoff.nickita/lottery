package ru.yagnenkoff.lottery.domain;

public record WinnerResponse(String name, Integer age, String city, Long winningAmount) {
}
