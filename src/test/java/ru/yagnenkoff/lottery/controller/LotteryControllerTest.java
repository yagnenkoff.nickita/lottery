package ru.yagnenkoff.lottery.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import ru.yagnenkoff.lottery.domain.Participant;
import ru.yagnenkoff.lottery.domain.WinnerResponse;
import ru.yagnenkoff.lottery.dto.ErrorResponseDto;
import ru.yagnenkoff.lottery.dto.ParticipantDto;
import ru.yagnenkoff.lottery.dto.WinnerResponseDto;
import ru.yagnenkoff.lottery.service.GameService;
import ru.yagnenkoff.lottery.service.ParticipantService;
import ru.yagnenkoff.lottery.service.WinnerService;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.yagnenkoff.lottery.constants.TestConstants.*;

@WebMvcTest(LotteryController.class)
@ActiveProfiles("test-local")
class LotteryControllerTest {

    private static final String urlPrefix = "/lottery/";

    @MockBean
    private ParticipantService participantService;
    @MockBean
    private WinnerService winnerService;
    @MockBean
    private GameService gameService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;


    @Test
    @SneakyThrows
    void shouldSuccessfullyRegisterParticipant() {
        final var participant = new ParticipantDto(NAME, AGE, CITY);

        Mockito.doNothing().when(participantService).register(Mockito.any());
        mockMvc.perform(post(urlPrefix + RequestMap.REGISTER_REQUEST_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(participant)))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    @SneakyThrows
    void shouldNotRegisterParticipantWhenParticipantDataNotValid() {
        final var participant = new ParticipantDto(NAME, 17, CITY);

        Mockito.doNothing().when(participantService).register(Mockito.any());
        final var mvcResult = mockMvc.perform(post(urlPrefix + RequestMap.REGISTER_REQUEST_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(participant)))
                .andExpect(status().isInternalServerError()).andReturn();
        final var errorResponse = objectMapper.readValue(mvcResult.getResponse()
                .getContentAsString(StandardCharsets.UTF_8), ErrorResponseDto.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), errorResponse.getStatusCode());

    }

    @Test
    @SneakyThrows
    void shouldSuccessfullyReturnParticipants() {
        final var participantDto = new ParticipantDto(NAME, AGE, CITY);
        final var expectedResponse = List.of(participantDto);
        final var participant = new Participant(NAME, AGE, CITY);

        Mockito.when(participantService.getParticipants())
                .thenReturn(List.of(participant));
        final var mvcResult = mockMvc.perform(get(urlPrefix + RequestMap.GET_PARTICIPANTS_REQUEST_URL))
                .andExpect(status().isOk()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expectedResponse));
    }

    @Test
    @SneakyThrows
    void shouldSuccessfullyStartGame() {
        final var expectedResponse = new WinnerResponseDto(NAME, AGE, CITY, WINNING_AMOUNT);
        final var winnerResponse = new WinnerResponse(NAME, AGE, CITY, WINNING_AMOUNT);

        Mockito.when(gameService.startGame())
                .thenReturn(winnerResponse);
        final var mvcResult = mockMvc.perform(get(urlPrefix + RequestMap.START_GAME_REQUEST_URL))
                .andExpect(status().isOk()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expectedResponse));
    }

    @Test
    @SneakyThrows
    void shouldSuccessfullyReturnWinners() {
        final var winnerResponseDto = new WinnerResponseDto(NAME, AGE, CITY, WINNING_AMOUNT);
        final var expectedResponse = List.of(winnerResponseDto);
        final var winnerResponse = new WinnerResponse(NAME, AGE, CITY, WINNING_AMOUNT);

        Mockito.when(winnerService.getWinners())
                .thenReturn(List.of(winnerResponse));
        final var mvcResult = mockMvc.perform(get(urlPrefix + RequestMap.GET_WINNERS_REQUEST_URL))
                .andExpect(status().isOk()).andReturn();
        assertThat(mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8))
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(expectedResponse));
    }

    @SneakyThrows
    private String asJsonString(final Object obj) {
        return objectMapper.writeValueAsString(obj);
    }
}