package ru.yagnenkoff.lottery.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.yagnenkoff.lottery.entity.WinnerEntity;
import ru.yagnenkoff.lottery.repository.WinnerRepository;
import ru.yagnenkoff.lottery.service.WinnerService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.yagnenkoff.lottery.constants.TestConstants.AGE;
import static ru.yagnenkoff.lottery.constants.TestConstants.CITY;
import static ru.yagnenkoff.lottery.constants.TestConstants.NAME;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNING_AMOUNT;

class WinnerServiceImplTest {

    private final WinnerRepository winnerRepository = Mockito.mock(WinnerRepository.class);
    private final WinnerService winnerService = new WinnerServiceImpl(winnerRepository);

    private final WinnerEntity winner = new WinnerEntity();

    @BeforeEach
    void setUp() {
        winner.setName(NAME);
        winner.setAge(AGE);
        winner.setCity(CITY);
        winner.setWinningAmount(WINNING_AMOUNT);
    }

    @Test
    void shouldReturnWinnersWhenTableWithWinnersNotEmpty() {
        Mockito.when(winnerRepository.findAll())
                .thenReturn(List.of(winner));

        final var winners = winnerService.getWinners();
        assertEquals(1, winners.size());

        winners.stream().findFirst()
                .ifPresentOrElse(winnerResponse -> {
                    assertEquals(NAME, winnerResponse.name());
                    assertEquals(AGE, winnerResponse.age());
                    assertEquals(CITY, winnerResponse.city());
                    assertEquals(WINNING_AMOUNT, winnerResponse.winningAmount());
                }, Assertions::fail);
    }
}