package ru.yagnenkoff.lottery.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.yagnenkoff.lottery.entity.ParticipantEntity;
import ru.yagnenkoff.lottery.entity.WinnerEntity;
import ru.yagnenkoff.lottery.exception.GameException;
import ru.yagnenkoff.lottery.repository.ParticipantRepository;
import ru.yagnenkoff.lottery.repository.WinnerRepository;
import ru.yagnenkoff.lottery.service.GameService;
import ru.yagnenkoff.lottery.service.RandomGeneratorService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static ru.yagnenkoff.lottery.constants.TestConstants.AGE;
import static ru.yagnenkoff.lottery.constants.TestConstants.CITY;
import static ru.yagnenkoff.lottery.constants.TestConstants.CORRECT_PARTICIPANTS_COUNT;
import static ru.yagnenkoff.lottery.constants.TestConstants.INCORRECT_PARTICIPANTS_COUNT;
import static ru.yagnenkoff.lottery.constants.TestConstants.NAME;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNER_NUMBER;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNING_AMOUNT;

class GameServiceImplTest {

    private final ParticipantRepository participantRepository = Mockito.mock(ParticipantRepository.class);
    private final WinnerRepository winnerRepository = Mockito.mock(WinnerRepository.class);
    private final RandomGeneratorService randomGeneratorService = Mockito.mock(RandomGeneratorService.class);
    private final GameService gameService = new GameServiceImpl(participantRepository, winnerRepository, randomGeneratorService);

    private final ParticipantEntity participant = new ParticipantEntity();
    private final WinnerEntity winner = new WinnerEntity();

    @BeforeEach
    void setUp() {
        participant.setName(NAME);
        participant.setAge(AGE);
        participant.setCity(CITY);

        winner.setName(NAME);
        winner.setAge(AGE);
        winner.setCity(CITY);
        winner.setWinningAmount(WINNING_AMOUNT);
    }

    @Test
    void shouldReturnWinnerResponseWhenGameWasSuccessful() {
        Mockito.when(participantRepository.count())
                .thenReturn(CORRECT_PARTICIPANTS_COUNT);
        Mockito.when(randomGeneratorService.generateWinner(Mockito.anyLong()))
                .thenReturn(WINNER_NUMBER);
        Mockito.when(participantRepository.getParticipantEntityByRowNumber(Mockito.anyLong()))
                .thenReturn(participant);
        Mockito.when(randomGeneratorService.generateWinningAmount())
                .thenReturn(WINNING_AMOUNT);
        Mockito.doNothing().when(participantRepository).deleteAll();
        Mockito.when(winnerRepository.save(Mockito.any()))
                .thenReturn(winner);

        final var winnerResponse = gameService.startGame();
        assertEquals(NAME, winnerResponse.name());
        assertEquals(AGE, winnerResponse.age());
        assertEquals(CITY, winnerResponse.city());
        assertEquals(WINNING_AMOUNT, winnerResponse.winningAmount());
    }

    @Test
    void shouldThrowExceptionWhenParticipantsCountLessTwo() {
        Mockito.when(participantRepository.count())
                .thenReturn(INCORRECT_PARTICIPANTS_COUNT);

        assertThrows(GameException.class, gameService::startGame);
    }
}