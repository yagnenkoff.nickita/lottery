package ru.yagnenkoff.lottery.service.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.yagnenkoff.lottery.domain.Participant;
import ru.yagnenkoff.lottery.entity.ParticipantEntity;
import ru.yagnenkoff.lottery.repository.ParticipantRepository;
import ru.yagnenkoff.lottery.service.ParticipantService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static ru.yagnenkoff.lottery.constants.TestConstants.AGE;
import static ru.yagnenkoff.lottery.constants.TestConstants.CITY;
import static ru.yagnenkoff.lottery.constants.TestConstants.NAME;

class ParticipantServiceImplTest {

    private final ParticipantRepository participantRepository = Mockito.mock(ParticipantRepository.class);
    private final ParticipantService participantService = new ParticipantServiceImpl(participantRepository);

    private final ParticipantEntity participantEntity = new ParticipantEntity();
    private final Participant participant = new Participant(NAME, AGE, CITY);

    @BeforeEach
    void setUp() {
        participantEntity.setName(NAME);
        participantEntity.setAge(AGE);
        participantEntity.setCity(CITY);
    }

    @Test
    void shouldNotThrowExceptionWhenUserRegisterWithCorrectData() {
        Mockito.when(participantRepository.save(Mockito.any()))
                .thenReturn(participantEntity);

        Assertions.assertDoesNotThrow(() -> participantService.register(participant));
    }

    @Test
    void shouldReturnParticipantsListWhenTableWithParticipantsNotEmpty() {
        Mockito.when(participantRepository.findAll())
                .thenReturn(List.of(participantEntity));

        final var participants = participantService.getParticipants();
        assertEquals(1, participants.size());

        participants.stream().findFirst()
                .ifPresentOrElse(participantResponse -> {
                    assertEquals(NAME, participantResponse.name());
                    assertEquals(AGE, participantResponse.age());
                    assertEquals(CITY, participantResponse.city());
                }, Assertions::fail);
    }
}