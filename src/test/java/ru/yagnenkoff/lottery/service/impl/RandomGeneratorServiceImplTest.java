package ru.yagnenkoff.lottery.service.impl;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.web.client.RestTemplate;
import ru.yagnenkoff.lottery.service.RandomGeneratorService;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static ru.yagnenkoff.lottery.constants.TestConstants.NUMBER_OF_PARTICIPANTS;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNER_NUMBER;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNER_RESPONSE;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNING_AMOUNT;
import static ru.yagnenkoff.lottery.constants.TestConstants.WINNING_AMOUNT_RESPONSE;

class RandomGeneratorServiceImplTest {

    private final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
    private final RandomGeneratorService randomGeneratorService = new RandomGeneratorServiceImpl(restTemplate);

    @Test
    @SuppressWarnings("unchecked")
    void generateWinner() {
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class), Mockito.any(Map.class)))
                .thenReturn(WINNER_RESPONSE);
        assertEquals(WINNER_NUMBER, randomGeneratorService.generateWinner(NUMBER_OF_PARTICIPANTS));
    }

    @Test
    @SuppressWarnings("unchecked")
    void generateWinningAmount() {
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.any(Class.class), Mockito.any(Map.class)))
                .thenReturn(WINNING_AMOUNT_RESPONSE);
        assertEquals(WINNING_AMOUNT, randomGeneratorService.generateWinningAmount());
    }
}