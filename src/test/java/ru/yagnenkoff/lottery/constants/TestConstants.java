package ru.yagnenkoff.lottery.constants;

public interface TestConstants {
    long WINNER_NUMBER = 2L;
    long WINNING_AMOUNT = 1000L;
    long CORRECT_PARTICIPANTS_COUNT = 2L;
    long INCORRECT_PARTICIPANTS_COUNT = 1L;
    long NUMBER_OF_PARTICIPANTS = 1000L;

    String WINNING_AMOUNT_RESPONSE = "1000\n";
    String WINNER_RESPONSE = "2\n";

    String NAME = "testName";
    Integer AGE = 18;
    String CITY = "testCity";

}
