FROM openjdk:17-alpine
COPY target/*.jar lottery.jar
ENTRYPOINT ["java","-jar","/lottery.jar"]