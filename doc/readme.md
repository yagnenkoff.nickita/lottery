# Лотерея

## Запуск приложения

Приложение представляет Spring Boot проект

Для сборки приложения используется сборщик Maven

Команда для сборки:

```
mvn package
```

В приложение добавлен Docker, после сборки проекта нужно запустить команду:

```
docker build --tag=lottery:latest . 
```

Для запуска окружения используется docker compose

Запуск производится командой:
```
docker compose --file docker-compose-local.yml up 
```
База данных будет доступна на порту 5432. Для подключения
неободимо использовать данные:
```
POSTGRES_USER: default_user
POSTGRES_PASSWORD: default_password
POSTGRES_DB: lottery

jdbc:postgresql://localhost:5432/lottery
```

Приложение будет доступно на порту 8080

## Тестирование приложения

Всего в приложении 4 эндпоинта

Для удобства тестировния в приложение добавлен Swagger

Для его вызова использовать адрес:

```
http://localhost:8080/swagger-ui/index.html
```

### Сценарий тестирования

- Через эндпоинт регистрации _/lottery/participant_ добавить несколько участников
- Вызвать эндпоинт _/lottery/participant_ для просмотра участников
- Провести лотерею вызовом _/lottery/start_
- Вывести победителей _/lottery/winners_






